'use strict';

/**
 * @ngdoc overview
 * @name sunApp
 * @description
 * # sunApp
 *
 * Main module of the application.
 */
angular
  .module('sunApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/formation', {
        templateUrl: 'views/education/formation.html',
        controller: 'formationCtrl'
      })
      .when('/perso', {
        templateUrl: 'views/education/perso.html',
        controller: 'persoCtrl'
      })
      .when('/competence', {
        templateUrl: 'views/education/competence.html',
        controller:  'competenceCtrl'
      })
      .when('/experience', {
        templateUrl: 'views/education/experience.html',
        controller:  'experienceCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
