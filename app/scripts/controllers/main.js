'use strict';

/**
 * @ngdoc function
 * @name sunApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sunApp
 */
angular.module('sunApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
      /* $scope.toto = "angular"*/
      /* {{toto}} <input type="text" ng-model="toto"/>   <button ng-click="getstatus("Newtoto")=ok"/>*/
      /*$scope.getstatus=function(n) {
			$scope.toto= n;

      }*/
    ];
    $scope.toto = "angular";
    $scope.getstatus=function(n) {
    	$scope.toto = n;
    }
    
  });
