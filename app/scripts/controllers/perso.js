'use strict';

/**
 * @ngdoc function
 * @name sunApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sunApp
 */
angular.module('sunApp')
  .controller('persoCtrl', function($scope, $http) {
    	$scope.perso="";
    	$http.get("assets/perso.json").success(function(data) {
    		$scope.perso = data;
    		console.log(data);
    	}).error(function(error) {
    		console.log(error);
    	});
    });

