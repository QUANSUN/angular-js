'use strict';

/**
 * @ngdoc function
 * @name sunApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sunApp
 */
angular.module('sunApp')
  .controller('competenceCtrl', function($scope, $http) {
    	$scope.competence="";
    	$http.get("assets/competence.json").success(function(data) {
    		$scope.competence = data;
    	}).error(function(error) {
    		console.log(error);
    	});
    });

