'use strict';

/**
 * @ngdoc function
 * @name sunApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sunApp
 */
angular.module('sunApp')
  .controller('experienceCtrl', function($scope, $http) {
    	$scope.experience="";
    	$http.get("assets/experience.json").success(function(data) {
    		$scope.experience = data;
    	}).error(function(error) {
    		console.log(error);
    	});
    });

