'use strict';

/**
 * @ngdoc function
 * @name sunApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the sunApp
 */
angular.module('sunApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
