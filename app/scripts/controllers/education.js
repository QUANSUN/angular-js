'use strict';

/**
 * @ngdoc function
 * @name sunApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sunApp
 */
angular.module('sunApp')
  .controller('formationCtrl', function($scope, $http) {
    	$scope.education="";
    	$http.get("assets/education.json").success(function(data) {
    		$scope.education = data;
    	}).error(function(error) {
    		console.log(error);
    	});
    });

